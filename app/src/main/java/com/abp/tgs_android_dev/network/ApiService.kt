package com.abp.tgs_android_dev.network

import retrofit2.Call
import com.abp.tgs_android_dev.model.DataMain
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {

    @POST("destination")
    @FormUrlEncoded
    fun wisata(@Field("ofset") ofset:Int, @Field("limit") limit:Int):Call<DataMain>
}