package com.abp.tgs_android_dev.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abp.tgs_android_dev.model.DataMain
import com.abp.tgs_android_dev.network.RestApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyViewModel : ViewModel(){
    private var data = MutableLiveData<DataMain>()
    private var status = MutableLiveData<Boolean>()
    init {
        getData(ofset = 0, limit = 3)
    }

    private fun getData(ofset:Int, limit:Int){
        status.value = true

        RestApi().api().wisata(ofset, limit).enqueue(object : Callback<DataMain>{
            override fun onFailure(call: Call<DataMain>, t: Throwable) {
                status.value = true
                data.value = null
            }

            override fun onResponse(call: Call<DataMain>, response: Response<DataMain>) {
                status.value = false

                if (response.isSuccessful){
                    data.value = response.body()
                } else {
                    status.value = true
                }
            }
        })
    }
    fun setData() : MutableLiveData<DataMain>{
        return data
    }

    fun getStatus():MutableLiveData<Boolean>{
        status.value = true
        return status
    }
}