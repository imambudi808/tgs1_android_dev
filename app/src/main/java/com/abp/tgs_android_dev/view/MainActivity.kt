package com.abp.tgs_android_dev.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.abp.tgs_android_dev.R
import com.abp.tgs_android_dev.adapter.DataAdapter
import com.abp.tgs_android_dev.model.DataWisata
import com.abp.tgs_android_dev.viewmodel.MyViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var  viewModel : MyViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

        viewModel.getStatus().observe(
            this, Observer {
                t ->
                if (t ?: true){
                    rv_wisata.visibility = View.GONE
                } else {
                    rv_wisata.visibility = View.VISIBLE
                }
            }
        )
        viewModel.setData().observe(this, Observer {
            t ->
            t?.data?.let { showData(it) }
        })
    }

    private fun showData(data: List<DataWisata>){
        rv_wisata.adapter = DataAdapter(data)
        rv_wisata.layoutManager = LinearLayoutManager(this)
        Log.d("isi : ", data.toString())
    }
}
